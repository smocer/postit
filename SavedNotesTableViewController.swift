//
//  SavedNotesTableViewController.swift
//  PostIt!
//
//  Created by Admin on 02.04.17.
//  Copyright © 2017 MAI. All rights reserved.
//

import UIKit
import CoreData

typealias NoteInfo = (text: String, urls: [Data], date: Date)

private let segueId = "show note details"

class SavedNotesTableViewController: UITableViewController {
    
    lazy var savedNotes: SavedNotesDictionary = Integrator.savedNotesFetch()
    
    var currentNote: NoteInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return savedNotes.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseID", for: indexPath) as! PostTableViewCell
        let noteInfo = savedNotes[indexPath.row]
        let text = noteInfo!.text
        let urls = noteInfo!.urls
        let date = noteInfo!.date
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        let stringDate = dateFormatter.string(from: date)
        
        cell.postText.text = text
        cell.photosAmount.text = urls.count == 0 ? "" : "...и \(urls.count) фото."
        cell.dateLabel.text = "Сохранено \(stringDate)"

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentNote = savedNotes[indexPath.row]
        performSegue(withIdentifier: segueId, sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nvc = segue.destination as? NoteViewController {
            nvc.note = currentNote
        }
    }
}
