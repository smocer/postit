//
//  PostTableViewCell.swift
//  PostIt!
//
//  Created by Admin on 02.04.17.
//  Copyright © 2017 MAI. All rights reserved.
//

import UIKit



class PostTableViewCell: UITableViewCell {

    @IBOutlet weak var postText: UILabel!
    @IBOutlet weak var photosAmount: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
