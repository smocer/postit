//
//  NoteViewController.swift
//  PostIt!
//
//  Created by Admin on 18.05.17.
//  Copyright © 2017 MAI. All rights reserved.
//

import UIKit

class NoteViewController: UIViewController {

    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var picture1: UIImageView!
    @IBOutlet weak var picture2: UIImageView!
    @IBOutlet weak var picture3: UIImageView!
    @IBOutlet weak var picture4: UIImageView!
    
    var note: NoteInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "use this note" {
            if let svc = segue.destination as? SocialViewController {
                svc.text = textLabel.text
                if let urls = note?.urls {
                    svc.photoUrls2 = urls
                }
            }
        }
    }
    
    func configure() {
        for view in view.subviews {
            if let imgView = view as? UIImageView {
                imgView.layer.borderColor = UIColor.black.cgColor
                imgView.image = UIImage(named: "placeholder")
                imgView.backgroundColor = UIColor.white
            }
        }
        
        if let noteInfo = note {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
            let stringDate = dateFormatter.string(from: noteInfo.date)
            
            textLabel.text = noteInfo.text
            dateLabel.text = "Сохранено \(stringDate)"
            var i = 20
            for photoURL in noteInfo.urls {
                if let preview = self.view.viewWithTag(i) as? UIImageView {
                    preview.image = UIImage(data: photoURL)
                    i += 1
                }
            }
        }
        
    }
    
}
