//
//  ViewController.swift
//  SocialIntegrator
//
//  Created by Admin on 06.03.17.
//  Copyright © 2017 MAI. All rights reserved.
//

import UIKit
import MobileCoreServices
import SwiftyVK
import AssetsLibrary

private struct SegueIdentifier {
    static let LogoutButton = "show logout button"
    static let SavedNotes = "show saved notes"
    static let PostScheduler = "show datepicker"
}

class SocialViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, VKDelegate, UIPopoverPresentationControllerDelegate {
    
    //# MARK: - outlets and var's/let's
    //# TODO: didSet etc
    @IBOutlet weak var postTextView: UITextView!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var twitterButton: UIButton!
    @IBOutlet weak var vkButton: UIButton!
    @IBOutlet weak var preview1: UIImageView!
    @IBOutlet weak var preview2: UIImageView!
    @IBOutlet weak var preview3: UIImageView!
    @IBOutlet weak var preview4: UIImageView!
    
    // VK
    let appID = "5929945"
    let scope: Set<VK.Scope> = [.wall, .photos]
    var currentUserID: String?
    //end VK
    
    lazy var savedNotes: SavedNotesDictionary = Integrator.savedNotesFetch()
    
    var text: String?
    var photoUrls = [Data]()
    var photoUrls2: [Data]?
    var photos = [UIImage]() {
        didSet {
            var i = 10
            
            for j in 0...4 {
                if let preview = self.view.viewWithTag(i + j) as? UIImageView {
                    preview.image = UIImage(named: "placeholder")
                }
            }
            
            for photo in photos {
                if let preview = self.view.viewWithTag(i) as? UIImageView {
                    preview.image = photo
                    i += 1
                }
            }
        }
    }
    
    //# MARK: - segues
    @IBAction func vkLongPressure(_ sender: UILongPressGestureRecognizer) {
        if sender.state == .began {
            performSegue(withIdentifier: SegueIdentifier.LogoutButton, sender: self)
        }
    }
    
    @IBAction func schedulePost(_ sender: Any) {
        performSegue(withIdentifier: SegueIdentifier.PostScheduler, sender: self)
    }
    
    @IBAction func goToSavedNotes(_ sender: Any) {
        performSegue(withIdentifier: SegueIdentifier.SavedNotes, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifier.LogoutButton {
            if let ppc = segue.destination.popoverPresentationController {
                ppc.delegate = self
            }
        }
        if let svc = segue.destination as? SchedulingViewController {
            svc.noteinfo = NoteInfo(postTextView.text, photoUrls, Date())
        }
    }
    
    //# MARK: - saving the draft to the database
    
    
    @IBAction func saveNote(_ sender: Any) {
        savedNotes[savedNotes.count] = (postTextView.text, photoUrls, Date())
        Integrator.saveNotes(savedNotes)
        showAlert(title: "Готово!", message: "Добавлено в закладки.")
    }
    
    @IBAction func clear(_ sender: Any) {
        postTextView.text = "";
        photos = [UIImage]()
    }
    
    
    //# MARK: - photo handling
    
    @IBAction func attachPhoto(_ sender: AnyObject) {
        if photos.count < 4 {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let picker = UIImagePickerController()
                picker.sourceType = .camera
                picker.mediaTypes = [kUTTypeImage as String]
                picker.delegate = self
                picker.allowsEditing = true
                present(picker, animated: true, completion: nil)
            } else {
                showAlert(title: "Can't take a photo :(", message: "Camera is not available on this device.")
            }
        } else {
            showAlert(title: "Ошибка", message: "Нельзя прикреплять больше 4 фотографий к публикации.")
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var image = info[UIImagePickerControllerEditedImage] as? UIImage
        if image == nil {
            image = info[UIImagePickerControllerOriginalImage] as? UIImage
        }

        switch photos.count {
        case 0:
            preview1.image = image
        case 1:
            preview2.image = image
        case 2:
            preview3.image = image
        case 3:
            preview4.image = image
        default:
            break
        }
        photos.append(image!)
        
        photoUrls.append(UIImagePNGRepresentation(image!)!)
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    //# MARK: - view lifecycle & configuring
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        NotificationCenter.default.addObserver(self, selector: #selector(self.alertSuccess), name: NSNotification.Name(rawValue: "postedSuccessfully"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateUI), name: NSNotification.Name(rawValue: "applicationWillEnterForeground"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateUI), name: NSNotification.Name(rawValue: "updateUI"), object: nil)
        //VK
        VK.config.logToConsole = true
        VK.configure(withAppId: appID, delegate: self)
        //end VK
    }
    

    func configureUI() {
        //textView
        postTextView.layer.cornerRadius = 8.0
        postTextView.layer.borderColor = UIColor(white: 0.75, alpha: 0.5).cgColor
        postTextView.layer.borderWidth = 1.2
        
        //photo previews
        func configurePreview(preview: UIImageView) {
            preview.layer.borderColor = UIColor.black.cgColor
            preview.image = UIImage(named: "placeholder")
            preview.backgroundColor = UIColor.white
        }
        configurePreview(preview: preview1)
        configurePreview(preview: preview2)
        configurePreview(preview: preview3)
        configurePreview(preview: preview4)

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func updateUI() {
        let isFacebookAvailable = Integrator.userHasLoggedIn(.facebook)
        let isTwitterAvailable = Integrator.userHasLoggedIn(.twitter)
        let isVkontakteAvailable = Integrator.userHasLoggedIn(.vkontakte)
        
        facebookButton.switchToState(isFacebookAvailable, networkType: .facebook)
        twitterButton.switchToState(isTwitterAvailable, networkType: .twitter)
        vkButton.switchToState(isVkontakteAvailable, networkType: .vkontakte)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let msg = text {
            postTextView.text = msg
        }
        if let data = photoUrls2 {
            photos = [UIImage]()
            for rawPhoto in data {
                photos.append(UIImage(data: rawPhoto)!)
            }
        }
        text = nil
        photoUrls2 = nil
        
        updateUI()
        
        if let navcon = self.navigationController {
            navcon.setViewControllers([self], animated: true)
        }
        
        print("!!!!!!!!viewWillAppear!!!!!!!!!");
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        updateUI()
    }
    
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //# MARK: - sending message (including handlers)
    
    @IBAction func sendToFacebook(_ sender: UIButton) {
        sendTo(sender.tag, network: .facebook)
    }
    
    @IBAction func sendToTwitter(_ sender: UIButton) {
        sendTo(sender.tag, network: .twitter)
    }
    
    @IBAction func sendToVK(_ sender: UIButton) {
        sendTo(sender.tag, network: .vkontakte)
    }
    
    func sendTo(_ tag: Int, network: SocialNetworkType) {
        
        if postTextView.text == "" && photos.isEmpty {
            showAlert(title: "Нечего отправлять.", message: "Вы пытаетесь отправить пустую публикацию.")
        } else {
        
            if tag == 1 {
                if network != .vkontakte {
                    if (network == .twitter) && (postTextView.text.characters.count > 140) {
                        let message = postTextView.text!
                        let numberBefore = message.characters.count / 136
                        var theRealLength = numberBefore * 4 + message.characters.count
                        let numberAfter = Int(ceil(Double(theRealLength) / 136.0))
                        
                        print(Double(message.characters.count) / Double(numberBefore))
                        
                        if numberAfter > numberBefore {
                            theRealLength += 4
                        }
                        
                        var separatedMessages = [String]()
                        let perMessage = message.characters.count / numberAfter
                        
                        var startIndex: String.Index = message.startIndex
                        var endIndex: String.Index = message.startIndex
                        
                        for i in 1...numberAfter {
                            startIndex = message.index(message.startIndex, offsetBy: (i - 1) * perMessage)
                            endIndex = message.index(startIndex, offsetBy: perMessage)
                            separatedMessages.append("\(i)/\(numberAfter) " + message.substring(with: startIndex..<endIndex))
                        }
                        
                        if endIndex != message.endIndex {
                            separatedMessages[separatedMessages.count - 1].append(message.substring(from: endIndex))
                        }
                        
                        
                        var secs = 0
                        
                        for str in separatedMessages {
                            let delay = DispatchTime.now() + .seconds(secs)
                            
                            DispatchQueue.main.asyncAfter(deadline: delay, execute: {[unowned self] in if let vc = Integrator.post(str, photos: self.photos, toNetwork: network, userIDForVK: nil) {
                                self.present(vc, animated: true, completion: nil)
                                } })
                            
                            secs += 5
                        }
                    } else {
                        if let vc = Integrator.post(postTextView.text, photos: photos, toNetwork: network, userIDForVK: nil) {
                            present(vc, animated: true, completion: nil)
                        }
                    }
                } else {
                    _ = Integrator.post(postTextView.text, photos: photos, toNetwork: .vkontakte, userIDForVK: currentUserID)
                }
            } else {
                if network == .vkontakte {
                    if VK.state == .authorized {
                        updateUI()
                    } else {
                        VK.logIn()
                    }
                } else {
                    showAlert(title: "Невозможно отправить :(", message: "Вам необходимо войти в \(network), чтобы делиться публикациями.")
                }
            }
        }
    }
    
    //не работает
    func alertSuccess(_ notification: NSNotification) {
        showAlert(title: "Успешно!", message: "Публикация отправлена.")
    }
    
    //# MARK: - VK!!!!!
    
    func vkWillAuthorize() -> Set<VK.Scope> {
        //Called when SwiftyVK need authorization permissions.
        return scope
    }
    
    func vkDidAuthorizeWith(parameters: Dictionary<String, String>) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "TestVkDidAuthorize"), object: nil)
        currentUserID = parameters["user_id"]
    }
    
    func vkAutorizationFailedWith(error: AuthError) {
        print("Autorization failed with error: \n\(error)")
        NotificationCenter.default.post(name: Notification.Name(rawValue: "TestVkDidNotAuthorize"), object: nil)
    }
    
    func vkDidUnauthorize() {}
    
    func vkShouldUseTokenPath() -> String? {
        return nil
    }
    
    func vkWillPresentView() -> UIViewController {
        return self
    }

    
    //# END VK
    
}

public extension UIViewController {
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

//# MARK: - extension
private extension UIButton {
    func switchToState(_ state: Bool, networkType: SocialNetworkType) {
        if state {
            self.tag = 1
            switch networkType {
            case .facebook:
                self.setImage(UIImage(named: "facebook"), for: UIControlState())
            case .twitter:
                self.setImage(UIImage(named: "twitter"), for: UIControlState())
            case .vkontakte:
                self.setImage(UIImage(named: "vk"), for: UIControlState())
            }
        } else {
            self.tag = 0
            switch networkType {
            case .facebook:
                self.setImage(UIImage(named: "facebook_black"), for: UIControlState())
            case .twitter:
                self.setImage(UIImage(named: "twitter_black"), for: UIControlState())
            case .vkontakte:
                self.setImage(UIImage(named: "vk_black"), for: UIControlState())
            }
        }
    }
}

