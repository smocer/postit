//
//  SchedulingViewController.swift
//  PostIt!
//
//  Created by Admin on 25.05.17.
//  Copyright © 2017 MAI. All rights reserved.
//

import UIKit

class SchedulingViewController: UIViewController {
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var noteinfo: NoteInfo?
    
    @IBAction func doneTapped() {
        let notification = UILocalNotification()
        var text = noteinfo!.text
        if text.characters.count > 10 {
            text = text.substring(with: text.startIndex..<text.index(text.startIndex, offsetBy: 11))
        }
        notification.alertBody = "Пришло время опубликовать запись \"\(text)\""
        notification.alertAction = "open"
        notification.fireDate = datePicker.date
        notification.userInfo = ["UUID": noteinfo!.date]
        notification.soundName = UILocalNotificationDefaultSoundName
        
        UIApplication.shared.scheduleLocalNotification(notification)
        
        showAlert(title: "Готово!", message: "Напоминание добавлено.")
    }

}
