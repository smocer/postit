//
//  AppDelegate.swift
//  PostIt!
//
//  Created by Admin on 20.03.17.
//  Copyright © 2017 MAI. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil))
        return true
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "applicationWillEnterForeground"), object: self)
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        application.windows.first!.rootViewController!.showAlert(title: "Напоминание", message: notification.alertBody!)
    }
    
}

