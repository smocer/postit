//
//  Integrator.swift
//  SocialIntegrator
//
//  Created by Admin on 07.03.17.
//  Copyright © 2017 MAI. All rights reserved.
//


import Social
import SwiftyVK

typealias SavedNotesDictionary = [Int : (text: String, urls: [Data], date: Date)]
typealias NSSavedNotesDictionary = [Int : [Int : Any]]

enum SocialNetworkType {
    case facebook
    case twitter
    case vkontakte
    
    var description: String {
        get {
            switch self {
            case .facebook:
                return "Facebook"
            case .twitter:
                return "Twitter"
            case .vkontakte:
                return "Вконтакте"
            }
        }
    }
}

class Integrator {
    
    static let savedNotesFetch = { () -> SavedNotesDictionary in
        let defaults = UserDefaults.standard
        if let rawData = defaults.object(forKey: "savedNotes") as? Data {
            let notes = NSKeyedUnarchiver.unarchiveObject(with: rawData) as! NSSavedNotesDictionary
            
            return dictionaryToTuple(notes)
        } else {
            return SavedNotesDictionary()
        }
    }
    
    static func saveNotes(_ notes: SavedNotesDictionary) {
        let defaults = UserDefaults.standard

        let notesAsRawData: Data = NSKeyedArchiver.archivedData(withRootObject: tupleToDictionary(notes))
        defaults.set(notesAsRawData, forKey: "savedNotes")
    }
    
    private static func tupleToDictionary(_ notes: SavedNotesDictionary) -> NSSavedNotesDictionary {
        var result = NSSavedNotesDictionary()
        
        for note in notes {
            let first = note.value.text
            let second = note.value.urls
            let third = note.value.date
            result[note.key] = [0 : first, 1 : second, 2 : third]
        }
        
        return result
    }
    
    private static func dictionaryToTuple(_ notes: NSSavedNotesDictionary) -> SavedNotesDictionary {
        var result = SavedNotesDictionary()
        
        for note in notes {
            let first = note.value[0] as! String
            let second = note.value[1] as! [Data]
            let third = note.value[2] as! Date
            result[note.key] = (text: first, urls: second, date: third)
        }
        
        return result
    }
    
    static func userHasLoggedIn(_ networkType: SocialNetworkType) -> Bool {
        switch networkType {
        case .facebook:
            return SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook)
        case .twitter:
            return SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter)
        case .vkontakte:
            if VK.state == .authorized {
                return true
            } else { return false }
        }
    }
    
    static func post(_ message: String, photos: [UIImage], toNetwork: SocialNetworkType, userIDForVK: String?) -> UIViewController? {
        switch toNetwork {
        case .twitter:
            if let vc = SLComposeViewController(forServiceType: SLServiceTypeTwitter) {
                return configurePost(message, photos: photos, vc: vc)
            } else { return nil }
        case .facebook:
            if let vc = SLComposeViewController(forServiceType: SLServiceTypeFacebook) {
                return configurePost(message, photos: photos, vc: vc)
            } else { return nil }
        case .vkontakte:
            if let userid = userIDForVK {
                sendToVK(message, photos: photos, userIDForVK: userid)
            } else {
                print("\n Error! U are not logged in.\n")
            }
            return nil
        }
    }
    
    fileprivate static func configurePost(_ message: String, photos: [UIImage], vc: SLComposeViewController) -> SLComposeViewController {
        vc.setInitialText(message)
        for photo in photos {
            vc.add(photo)
        }
        return vc
    }
    
    private static func sendToVK(_ message: String, photos: [UIImage], userIDForVK: String) {
        
        var wallPostParameters = Dictionary<VK.Arg, String>()
        wallPostParameters[VK.Arg.message] = message
        var attachments = ""
        var successfulUploadsCount = 0
        
        let sendPostWorkItem = DispatchWorkItem {
            VK.API.Wall.post(wallPostParameters).send(
                onSuccess: {response in print(response)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "postedSuccessfully"), object: self) },
            onError: {error in print(error)},
            onProgress: {done, total in print("\n send \(done) of \(total)\n")} )
        }
        
        for photo in photos {
            let media = Media(imageData: Data(UIImageJPEGRepresentation(photo, 1.0)!), type: Media.ImageType.JPG)
            
            VK.API.Upload.Photo.toWall.toUser(media, userId: userIDForVK).send(
                
                onSuccess: { response in
                    let photoID = response[0,"id"].stringValue
                    let photoString = "photo" + userIDForVK + "_" + photoID
                    
                    if attachments.isEmpty {
                        attachments = photoString
                    } else {
                        attachments += "," + photoString
                    }
                    successfulUploadsCount += 1
                    
                    if successfulUploadsCount == photos.count {
                        wallPostParameters[VK.Arg.attachments] = attachments
                        sendPostWorkItem.perform()
                    }
                    print("\n SwiftyVK: uploadPhoto success \n \(response)\n") },
                
                onError: { error in
                    print("\n SwiftyVK: uploadPhoto fail \n \(error)\n")
                    },
                
                onProgress: { done, total in
                    print("\n SwiftyVK: uploadPhoto progress: \(done) of \(total))\n") } )
        }
        
        if photos.isEmpty {
            sendPostWorkItem.perform()
        }
    }
    
}
