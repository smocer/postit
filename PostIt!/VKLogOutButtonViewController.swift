//
//  VKLogOutButtonViewController.swift
//  PostIt!
//
//  Created by Admin on 23.03.17.
//  Copyright © 2017 MAI. All rights reserved.
//

import UIKit
import SwiftyVK

class VKLogOutButtonViewController: UIViewController {

    @IBAction func logoutButton() {
        VK.logOut()
        //self.performSegue(withIdentifier: "close logout vc", sender: self)
        
        let delay = DispatchTime.now() + .seconds(3)
        
        DispatchQueue.main.asyncAfter(deadline: delay, execute: {[unowned self] in
            NotificationCenter.default.post(name: Notification.Name(rawValue: "updateUI"), object: self)
        })
    }
    
    /*override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let svc = segue.destination as? SocialViewController {
            svc.updateUI()
        }
    }*/
    
    override var preferredContentSize: CGSize {
        get {
            if presentingViewController != nil {
                return CGSize(width: 128, height: 128)
            } else { return super.preferredContentSize }
        }
        set { super.preferredContentSize = newValue }
    }

}
